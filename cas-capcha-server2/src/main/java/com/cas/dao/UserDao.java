package com.cas.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * @author Leighton
 * @create 2018-11-09.
 * 操作用户信息
 */
@Repository("userDao")
public class UserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Map<String,Object> findByUserName(String userName){
        return jdbcTemplate.queryForMap("SELECT * FROM user_info WHERE username=?",userName);
    }
}
