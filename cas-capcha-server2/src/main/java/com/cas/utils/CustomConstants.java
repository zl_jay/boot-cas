package com.cas.utils;

/**
 * @author Leighton
 * @create 2018-11-14.
 */
public class CustomConstants {
   public static final String  LOGIN_FIRST = "LOGIN_FIRST";

    public static final  String FIRST = "FIRST";

    public static final String NOTFIRST = "NOTFIRST";
//    UsernamePasswordCredential

    //验证码认证控制属性
    public  static final String CAPTCHA_AUTHENTICATION_ENABLED="captchaAuthenticationEnabled";
}
