package com.cas.service;

import java.util.Set;

/**
 * @author Leighton
 * @create 2018-11-09.
 */
public interface RoleService {
    String findRolesByUserId(String uid);

    Set<String> findAllRoles();
}
