package com.cas.exception;

import javax.security.auth.login.AccountException;

/**
 * 验证码错误异常类
 * @author Leighton
 * @create 2018-11-11.
 */
public class CaptchaErrorException extends AccountException {
}
